import React, {Component} from 'react';

export default class Navigation extends Component {
    render(){
        return(
            <nav className="Navigation">
                <div className="Navigation-logo">
                    <img src="" alt=""/>
                </div>
                <button className="Button Navigation-contact">Contacto</button>
            </nav>
        );
    }
}